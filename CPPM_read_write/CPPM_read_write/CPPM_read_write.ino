/*
 Name:		CPPM_read_write.ino
 Created:	16-May-18 10:07:54 AM
 Author:	Admin
*/


#include <PulsePosition.h>

// Simple loopback test: create 1 output to transmit
// test pulses, and 1 input to receive the pulses
PulsePositionOutput myOut;
PulsePositionInput myIn;

void setup() {
	Serial.begin(9600);
	myOut.begin(9);  // connect pins 9 and 10 together for loopback test
	myIn.begin(10);
	//myOut.write(0, 1000);
	myOut.write(1, 600.03);
	myOut.write(2, 1500);
	myOut.write(3, 759.24);
	// slots 4 and 5 will default to 1500 us
	myOut.write(6, 1234.56);
}

int count = 0;
float val1;
float val2;
float val3;
float val4;
float val5;

void loop() {
	int i, num;

	//oat val0 = myIn.read(0);
	val1 = myIn.read(1);
	val2 = myIn.read(2);
	val3 = myIn.read(3);
	val4 = myIn.read(4);
	val5 = myIn.read(5);
	//Serial.print(val0);
	Serial.print("  ");
	Serial.print(val1);
	Serial.print("  ");
	Serial.print(val2);
	Serial.print("  ");
	Serial.print(val3);
	Serial.print("  ");
	Serial.print(val4);
	Serial.print("  ");
	Serial.println(val5);
}


